import requests
import getApi as dec
import os
from bs4 import BeautifulSoup
from random import randint
from time import sleep
import header_s
import setting
import datetime
#from datetime import datetime
#import socks
#import socket
import urllib.request
#from urllib.request import urlopen
#define 
setting.init()
title = setting.title
diry = setting.diry
headers = header_s.getprofile()
setting.errorlog = setting.errorlog +str(datetime.datetime.now())[:-7]+'.log'
dir_badSample = setting.dir_badSample
global errorlog
errorlog  = setting.errorlog
html_timeout = setting.dwld_html_timeout
oper_timeout = setting.dwld_oper_timeout
global ip
#ip = dec.getip_next()

#print(header_s.file_1)
#####for test use
###url = 'https://www.whatismybrowser.com/detect/what-http-headers-is-my-browser-sending'
###
#dl_file('http://s1.nb-pintai.com/2014/09/011312480.jpg','a.jpg')
####sp = dl_html('http://icanhazip.com')
###print(dl_html(url).find('table').get_text)
def dl_html(ip,urlf):
    #global ip

    #while True:
    try:
        write_log("\tTryproxy : By "+ip)
        proxies = {"http":'http://'+ip,"https":'https://'+ip}
        html = requests.get(urlf,proxies = proxies,headers = headers ,timeout = html_timeout)
        #html = requests.get(urlf,headers = headers ,timeout = html_timeout)
        if html == None:raise Exception('HtmlNone')
        #print(html.text)
        sp = BeautifulSoup(html.text,'html.parser')
        if not check_sp(sp,ip):raise Exception('CheckSpFail')
        if sp.find('title').text == setting.gallary_not_available:
            write_meta_add(diry+setting.title+'/','Available','Gallery Not Available Anymore')
            write_log('Message : Gallery Not Available Anymore')
            raise Exception('NotAvailable')
    except Exception as e:
        write_log("\tFailure : "+ error_msg(e,ip))
        if e.args[0] == 'NotAvailable':
            return 'NotAvailable'
        
        #ip = dec.getip_next()
        sleep(10)
        return 'error'
    else:
        write_log("\tSuccess : By "+ip)
        #sleep_awhile()
        sleep(20)            
        #break
        return sp

def dl_file(ip,url,filepath):#############################more error warning
    #global ip
    #print(ip,url,filepath)
    #while True:    
    try:
            
            
        write_log("\tTryproxy : By "+ip)
        if ip != 'localhost':
            proxy = urllib.request.ProxyHandler({'http': ip})
            opener = urllib.request.build_opener(proxy)
        elif ip == 'localhost':
            opener = urllib.request.build_opener()
        opener.addheaders = get_headers_tuple()
        with opener.open(url,timeout = oper_timeout) as image:
            if os.path.exists(filepath):
                with open(filepath,'r') as f:
                    img_size = int(image.info()['Content-Length'])
                    file_size = int(f.seek(0,2))
                    if (img_size) == (file_size):
                        write_log('Warning : File : %s already exists (from %s )'%(filepath,url))
                        return 'success'
            for badimg in os.listdir(dir_badSample):
                with open(os.path.join(dir_badSample,badimg)) as f:
                    img_size = int(image.info()['Content-Length'])
                    file_size = int(f.seek(0,2))
                    if (img_size) == (file_size):
                        write_log('Warning : File : %s is not true file (from %s )'%(filepath,url))
                        return 'error'
            with open(filepath,'wb') as f:
                t = datetime.datetime.now()
                #sleep(3)
                img_size = int(image.info()['Content-Length'])
                dwn_buffer_size = setting.dwn_buffer_size
                while True:
                    buffer = (image.read(dwn_buffer_size))
                    sleep(setting.dwn_img_delay)
                    if not buffer :
                        break
                    else:
                        f.write(buffer)
                file_size = int(f.seek(0,2))
                if (img_size) != (file_size):
                    raise Exception('Download Error')
                t1 = datetime.datetime.now()
    except Exception as e:
        write_log("\tFailure : "+ error_msg(e,ip))
        #ip = dec.getip_next()
        return 'error'
        sleep(10)
    else:
        setting.dwn_total += file_size
        write_log("\tSuccess : By %s whick url size : %d , file size : %d ,cost time : %d sec"%(ip,img_size,file_size,(t1-t).seconds))
        sleep_awhile(ip)
        return 'success'
        
        
            




'''
import requests

s = requests.Session()
s.proxies = {"http": "http://195.138.83.218:53281"}

r = s.get("http://s1.nb-pintai.com/2014/09/011312480.jpg")
with open('b.jpg','wb') as f:
    f.write(r)
'''
def error_msg(e,ip):
    try:
        s = str(e.args[0])#[:str(e.args[0]).find('(')]
    except:
        s = e
    finally:
        return ip+'  ' + str(s)
def check_sp(sp,ip):
    global title
    t = sp.select('title')[0].text### appear list index out of range
    if sp.find('a',{'data-orig-proto':'https'})!=None: ###suffer recatpcha
        write_log('\tFailure : %s encounter a catpcha , fuck'%ip)  
        return False
    elif not (t.endswith('E-Hentai Galleries') or t.startswith(title)):
        write_log('\tWarning : Not correct page (catpcha?)')
        return False
    elif t == 'Attention Required! | Cloudflare' :
        title = ""
        write_log('\tWarning : encounter Cloudflare')
        return False
    elif t.endswith('Cache Access Denied'):
        write_log('\tWarning : Cache Access Denied')
        return False
    else:
        return True

def write_log(s):
    global errorlog
    #print('filename : ' +errorlog)
    if not os.path.exists(errorlog):
        with open(errorlog,'w') as f:
            f.write('======================================================\n')
            f.write('===========this is run log start from ================\n')
    with open(errorlog,'a') as f:
        f.write('[%s]:%s'%(str(datetime.datetime.now().time()),str(s)))
        f.write('\n')
    print(s)
def write_meta(filepath,result):
    with open(filepath+'metadata','w') as f:
        title,title_jpn,tags,rating,filesize,filecount,posted,token,url = result
        f.write('title = \t' + title+'\n')
        f.write('title_jpn = \t' + title_jpn+'\n')
        f.write('tags = \t' + str(tags) +'\n')
        f.write('rating = \t' + rating+'\n')
        f.write('filesize = \t' + str(filesize)+'\n')
        f.write('filecount = \t' + filecount +'\n')
        f.write('posted = \t' + posted+'\n')
        f.write('token = \t' + token+'\n')
        f.write('url = \t' + url+'\n')
def write_meta_add(filepath,entry,detail):
    with open(filepath+'metadata','a') as f:
        f.write('%s = \t%s\n'%(str(entry),str(detail)))
def get_headers_tuple():
    #print(header_s.file_1)
    return ( [(k,v) for k, v in header_s.file_1.items()])
def sleep_awhile(ip):
    t = setting.scs_slp_base + randint(0,setting.scs_slp_bias) 
    write_log('\tBreaktime : %s :Time to get a rest for poor spider : %d '%(ip,t))
    sleep(t)
#dl_file('http://s1.nb-pintai.com/2014/09/011312480.jpg','b.jpg')

###tor try fail
'''
def dl_html2(urlf):
    global ip
    proxies =dict(http='socks5://localhost:9050',https='socks5://localhost:9050')
    while True:
        try:
            socks.set_default_proxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)
            socket.socket = socks.socksocket
            html = requests.get(urlf,proxies = proxies,headers = headers ,timeout = html_timeout)
            print(html.text)
            if html == None:
                raise
            sp = BeautifulSoup(html.text,'html.parser')
            if not check_sp(sp):raise Exception
        except Exception as e:
            write_log(error_msg(e,ip))#?????????????????????????????for error_msg
            ip = dec.getip_next()
        else:
            break
        finally:
            sleep(3+randint(0,5))
    return sp

import requests


proxies = {
    'http': 'socks5://127.0.0.1:9050',
    'https': 'socks5://127.0.0.1:9050'
}


def main():
    url = 'http://ifconfig.me/ip'

    response = requests.get(url)
    print('ip: {}'.format(response.text.strip()))

    response = requests.get(url, proxies=proxies)
    print('tor ip: {}'.format(response.text.strip()))


if __name__ == '__main__':
    main()
'''
