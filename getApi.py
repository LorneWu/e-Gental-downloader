import requests
import os
from bs4 import BeautifulSoup
import datetime
import urllib.request as urllib2
import urllib.error 
import urllib.request 
import socket
from selenium import webdriver
from random import shuffle
import urllib
import setting
from time import sleep
setting.init()
diry = setting.diry
#serverlog = setting.serverlog
#filepath = setting.serverlist
reqs_tLim = setting.server_req_limit

################################for proxy search###############################
global itag
itag = 0
global server_list
server_list = []

def shown(func):
    def wrapper(obj):
        print(obj.url)
        print(obj.filepath)
        t = datetime.datetime.now()
        with open(obj.filepath,'w') as f:
            f.write('===='+str(t)+'\t'+obj.url+'\n')
            f.write('===========this is run log start from ===========\n')

        #print(filepath)
        count = func(obj)
        t1 = datetime.datetime.now()
        with open(obj.filepath,'a') as f:        
            f.write('=================================================================='+'\n')            
            f.write('=====update at '+str(t1)+'  url:%s'%(obj.url)+'\n')
            f.write('=====total cost time : %s seconds  ,total links is %s .'%(str((t1-t).seconds),count))
        #self.oInstance = Cls(*args,**kwargs)
        #print(count)
        #return count
    return wrapper
'''
def shown(func):
    def wrapper(a,url,filepath):
        print(a)
        print(url)
        print(filepath)
        write_log(filepath,'=====start the scan from %s now.'%url)
        write_log(filepath,'===================================================')
        count = func(a,url,filepath)
        write_log(filepath,'=====end the scan from %s now.'%url)
        #with open(filepath,'a')as f:
        write_log(filepath,'================================================================================ \n')
        write_log(filepath,'=====update at '+str(datetime.datetime.now())+'  %s count is %d'%(url,count)+'\n')
        write_log(filepath,'================================================================================ \n')
            #f.write('================================================================================ \n')
            #f.write('=====update at '+str(datetime.datetime.now())+'  %s count is %d'%(url,count)+'\n')
            #f.write('================================================================================ \n')
        return count
    return wrapper
'''
def server_list_init():
    global server_list
    server_list = get_server_list()
    shuffle(server_list)
    
def getip_next():
    if server_list == []:
        server_list_init()
    global itag
    r = server_list[itag]
    if itag+1 == len(server_list):
        print('WWWWWWWWWarning : Serverlist is run of')
        server_list_init()##############when the list get out of , re take it
    else:
        itag += 1
    return r
def get_server_time():##get the previous getserver list
    if os.path.exists(setting.serverlist):
        with open(setting.serverlist,'r') as f:
            tmp = f.readline()
            tmp = tmp[26:tmp.find('=',25)]
            result = []
            for i in tmp.split('-'):
                for j in i.split(' '):
                    for k in j.split(':'):
                        for l in k.split('.'):
                            result.append(int(l))
            return datetime.datetime(result[0],result[1],result[2],result[3],result[4],result[5],result[6])
    else:
        return datetime.datetime(2010,1,1)
#print(get_server_time())
#print (datetime.datetime.now() - get_server_time())
def get_server_list(path=setting.serverlist):
    result = []
    if os.path.exists(path):
        with open(path,'r') as f:
            for i in f.readlines():
                if i[:1] in ['='] or i[:3] in [' ==','===','  =']:
                    pass
                elif i[:i.find('\t')] not in result:
                    result.append(i[:i.find('\t')])
        return result
    else:
        sleep(10)
        return get_server_list(path)
def try_server(filepath,ip):
    try:
        proxyDict = {"http": ip,"https":ip ,"ftp":ip}
        url = setting.try_server_url
        requests.get(url,proxies = proxyDict,timeout=reqs_tLim)
    except IOError as e:
        write_log(filepath,'===='+ip + '\t Fail'+str(str(e.args[0])[:(str(e.args[0]).find('('))]))
        return False
    else:
        write_log(filepath,ip + '\t Success')
        sleep(1)
        return True

def write_log(filepath,s):
    global errorlog
    #print('filename : ' +errorlog)
    if not os.path.exists(filepath):
        with open(filepath,'w') as f:
            f.write('===='+str(datetime.datetime.now())+'\n')
            f.write('===========this is run log start from ===========\n')
    with open(filepath,'a') as f:
        f.write(str(s))
        f.write('\n')
    print(s)        
def print_state(filepath,ip,security,country,update='',protocol=''):
    s = "====%s,%s,%s \t;UpdateTime:%s Protocol:%s "%(ip,security,country,update,protocol)
    write_log(filepath,s)
    return s+'\n'
################################for using webdriver############################
def cnct_html_wd(url):

    ###use the firefox to get
    #driver = webdriver.Chrome(executable_path=r'/etc/webdriver/chromedriver')  # PhantomJs
    #driver = webdriver.Firefox(executable_path=r'/etc/webdriver/geckodriver')  # PhantomJs
    driver = webdriver.PhantomJS(executable_path='/etc/webdriver/phantomjs')
    driver.set_page_load_timeout(50)
    driver.get(url)
    pageSource = driver.page_source  # 取得網頁原始碼
    #print(pageSource)
    driver.close()
    sleep(15)
    return pageSource


def error_msg(e,ip='',t_error=''):
    try:
        s = str(e.args[0])[:str(e.args[0]).find('(')]
    except:
        s = e
    finally:
        return ((ip+'  'if ip!='' else '') +(str(t_error) +'  'if t_error!='' else '')+ str(s))
