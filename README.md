# pictureDownload

*** Program Detail ***

This is an E-Gental Downloader based on Linux-python3.6 environment.
And it will use these module : 

    requests,
    bs4,
    urllib.request,
    urllib.error,
    selenium,
    threading,
    _thread

You can give the link , no matter a single page , a gallary link or even a list of gallarys.
It can help u download the comics you want.

E-Gental is a well-known website and have huge comic books.
But it has a lot of limitation like download limitation per ip,
And use Catcha and Cloudflare to protect the website.

This program is based on catcha the image without account.
And avoid the limitation of the download by proxy server (by writing more spider work on the free proxy server list)
And using multithread , let more spider work at one time.
So it can download more than 1 file per minute.
And refresh the server list in the background.
Use the EhWiki-API to get the comic's metadata.(EhWiki-API is really useless...)

But this is limited by free proxy server website , that some of the server on the list is not stable for us.
