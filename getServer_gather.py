import requests
from bs4 import BeautifulSoup
import getApi as dec
#import setting
from datetime import datetime
import threading
url = 'http://www.gatherproxy.com/'
condition_Safe = ['Elite' ,'Anonymous']

def shown(func):
    def wrapper(obj):
        print(obj.url)
        print(obj.filepath)
        t = datetime.now()
        with open(obj.filepath,'w') as f:
            f.write('===='+str(t)+'\t'+obj.url+'\n')
            f.write('===========this is run log start from ===========\n')

        #print(filepath)
        count = func(obj)
        t1 = datetime.now()
        with open(obj.filepath,'a') as f:        
            f.write('=================================================================='+'\n')            
            f.write('=====update at '+str(t1)+'  url:%s'%(obj.url)+'\n')
            f.write('=====total cost time : %s seconds  ,total links is %s .'%(str((t1-t).seconds),count))
        #self.oInstance = Cls(*args,**kwargs)
        #print(count)
        #return count
    return wrapper

class getServer(threading.Thread):
    def __init__(self,url,filepath,queue,erqu):
        threading.Thread.__init__(self)
        self.url = url
        self.filepath = filepath
        self.queue = queue
        self.erqu = erqu
    @dec.shown    
    def run(self):
        #try:
        html = requests.get(self.url)
        sp = BeautifulSoup(html.text,'html.parser')
        
        count = 0
        result = sp.find_all('script',{'type':'text/javascript'})
        for line in result:
            if (line.text).find('gp.insertPrx')>0:
                #result2.append(line.text)
                tmp = line.text
                li = []

                for i in range(32):
                    start = tmp.find('"')
                    if start == -1: break
                    tmp = tmp[start+1:]
                    end = tmp.find('"')
                    if end == -1: break
                    #if (i+1)%2==0:
                    li.append(tmp[:end])
                    tmp = tmp[end+1:]
                    
                
                s = dec.print_state(self.filepath,li[5]+':'+str(int(li[9], 16)),li[18],li[3],li[7],"")
                
                
                if li[18] in condition_Safe  :
                     if dec.try_server(self.filepath,li[5]+':'+str(int(li[9], 16)))==True :
                        count+=1
                        with open(self.filepath,'a') as f:
                            f.write(s)
                li =[]
        #except Exception as e:
            #print(e)
            #self.erqu.put(e)
        #lse:
        #print(count)
        self.queue.put(count)
        return(count)
    #check_list_gather('http://www.gatherproxy.com/','tmp')
    
