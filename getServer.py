# -*- coding: utf-8 -*-
import os
import getApi as dec
import requests
from bs4 import BeautifulSoup
import getServer_freeProxy as s1
import getServer_idcloak as s2
import getServer_gather as s3
import getServer_proxylist as s4
from shutil import move
from datetime import datetime
import setting
import _thread
from queue import Queue
import queue

#from datetime import datetime
#import datetime
setting.init()
filepath = setting.serverlist
#import signal

def signal_handler(signum, frame):
    raise Exception("Timed out!")


'''
url = 'http://www.us-proxy.org/'
html = requests.get(url)
sp = BeautifulSoup(html.text,'html.parser')
'''
### get the html file now
'''
data_1 = sp.select('tbody')

print(type(data_1))
'''
#print(sp)

def init(filepath):
    print('Start the init process ... please wait ')
    queue = Queue()
    erqu = Queue()
    if not os.path.exists('log'):
        os.mkdir('log')
        
    path_1 = 'log/Gental_log_serverlist_s1.log'
    path_2 = 'log/Gental_log_serverlist_s2.log'
    path_3 = 'log/Gental_log_serverlist_s3.log'
    path_4 = 'log/Gental_log_serverlist_s4.log'
    path_5 = 'log/Gental_log_serverlist_s5.log'
    path_6 = 'log/Gental_log_serverlist_s6.log'
    path_7 = 'log/Gental_log_serverlist_s7.log'
    
    thread1 = s1.getServer('http://free-proxy-list.net/',path_1,queue,erqu)
    thread2 = s1.getServer('http://www.us-proxy.org/',path_2,queue,erqu)
    thread3 = s1.getServer('http://www.sslproxies.org/',path_3,queue,erqu)
    thread4 = s1.getServer('http://free-proxy-list.net/uk-proxy.html',path_4,queue,erqu)
    thread5 = s2.getServer('http://www.idcloak.com/proxylist/proxy-server-list.html',path_5,queue,erqu)
    thread6 = s3.getServer('http://www.gatherproxy.com/',path_6,queue,erqu)
    #thread7 = s4.getServer('https://proxy-list.org/english/search.php?search=&country=any&type=any&port=any&ssl=any',path_7,queue,erqu)
    t = datetime.now()
    #thread7.start()
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()
    thread5.start()
    thread6.start()
    
    thread1.join()
    thread2.join()
    thread3.join()
    thread4.join()
    thread5.join()
    thread6.join()
    #thread7.join()
    
    asum = 0
    s = (datetime.now()-t).seconds
    with open(filepath,'w') as f,open(path_1,'r')as f1,open(path_2,'r')as f2,open(path_3,'r')as f3,open(path_4,'r')as f4,open(path_5,'r')as f5,open(path_6,'r')as f6:#,open(path_7,'r')as f7:
        f.write('===============start at : '+str(t)+'=============================\n')
        f.write('=================================================================\n')
        f.write(f1.read())
        f.write(f2.read())
        f.write(f3.read())
        f.write(f4.read())
        f.write(f5.read())
        f.write(f6.read())
        #f.write(f7.read())
        f.write('=================================================================\n')
        f.write('===============end at : '+str(t)+'===============================\n')
        while not queue.empty():
            tmp = queue.get()
            f.write('===============source : %d   \n'%tmp)
            asum+=tmp
        f.write('=============== total resource : %d'%asum)
        f.write('=============== total time : %s sec\n'%str((datetime.now()-t).seconds))
        f.write('=================================================================\n')
    print('finish') 
def init_quick(filepath):
    print('Start the init_quick process ... please wait ')
    queue = Queue()
    erqu = Queue()
    if not os.path.exists('log'):
        os.mkdir('log')
        
    #path_1 = 'log/Gental_log_serverlist_s1.log'
    path_2 = 'log/Gental_log_serverlist_s2.log'
    path_3 = 'log/Gental_log_serverlist_s3.log'
    path_4 = 'log/Gental_log_serverlist_s4.log'
    path_5 = 'log/Gental_log_serverlist_s5.log'
    path_6 = 'log/Gental_log_serverlist_s6.log'
    #path_7 = 'log/Gental_log_serverlist_s7.log'
    
    #thread1 = s1.getServer('http://free-proxy-list.net/',path_1,queue,erqu)
    thread2 = s1.getServer('http://www.us-proxy.org/',path_2,queue,erqu)
    thread3 = s1.getServer('http://www.sslproxies.org/',path_3,queue,erqu)
    thread4 = s1.getServer('http://free-proxy-list.net/uk-proxy.html',path_4,queue,erqu)
    thread5 = s2.getServer('http://www.idcloak.com/proxylist/proxy-server-list.html',path_5,queue,erqu)
    thread6 = s3.getServer('http://www.gatherproxy.com/',path_6,queue,erqu)
    #thread7 = s4.getServer('https://proxy-list.org/english/search.php?search=&country=any&type=any&port=any&ssl=any',path_7,queue,erqu)
    
    t = datetime.now()
    
    #thread7.start()
    #thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()
    thread5.start()
    thread6.start()
    
    
    #thread1.join()
    thread2.join()
    thread3.join()
    thread4.join()
    thread5.join()
    thread6.join()
    #thread7.join()
    #print(queue.get())
    #print(queue.empty())
    
    asum = 0
    #print(queue.empty())
    #print(asum)
    s = (datetime.now()-t).seconds
    #print(s)
    
    with open(filepath,'w') as f,open(path_2,'r')as f2,open(path_3,'r')as f3,open(path_4,'r')as f4,open(path_6,'r')as f6,open(path_5,'r')as f5 :#,open(path_1,'r')as f1,open(path_7,'r')as f7:,
        f.write('===============start at : '+str(t)+'=============================\n')
        f.write('=================================================================\n')
        #f.write(f1.read())
        f.write(f2.read())
        f.write(f3.read())
        f.write(f4.read())
        f.write(f5.read())
        f.write(f6.read())
        #f.write(f7.read())
        f.write('=================================================================\n')
        f.write('===============end at : '+str(t)+'===============================\n')
        while not queue.empty():
            tmp = queue.get()
            f.write('===============source : %d   \n'%tmp)
            asum+=tmp
        f.write('=============== total resource : %d'%asum)
        f.write('=============== total time : %s sec\n'%str((datetime.now()-t).seconds))
        f.write('=================================================================\n')
    print('finish')        
def getNewServer():
    print('get in the getNewServer process now ... ')
    if os.path.exists(filepath):
        tmp = 'log/Gental_log_serverlist.tmp'
        init_quick(tmp)
        move(filepath,'log/'+filepath[:-4]+str(datetime.now())+'.log')
        move(tmp,filepath)
    else:
        init(filepath)

'''
result = get_server_list()

for i in result:
    try_server(i)
'''
#getNewServer()

'''
    asum  +=s1.check_list_USandUK_proxy('http://free-proxy-list.net/',filepath)
    asum  +=s1.check_list_USandUK_proxy('http://www.us-proxy.org/',filepath)
    asum  +=s1.check_list_USandUK_proxy('http://www.sslproxies.org/',filepath)
    asum  +=s1.check_list_USandUK_proxy('http://free-proxy-list.net/uk-proxy.html',filepath)
    asum  +=s2.check_list_idcloak_proxy('http://www.idcloak.com/proxylist/proxy-server-list.html',filepath)    
    asum  +=s3.check_list_gather('http://www.gatherproxy.com/',filepath)
    asum  +=s4.check_list_proxylist('https://proxy-list.org/english/search.php?search=&country=any&type=any&port=any&ssl=any',filepath)
'''

# need to use try 
  
#write log
#print(queue.get(),queue.get(),queue.get(),queue.get())
'''
while True:
    try: 
        exc = erqu.get(block=False)
    except queue.Empty:
        pass
    else:
        exc_type,exc_obj,exc_trace = exc
        print(exc_type)
        print(exc_obj)
        print(exc_trace)
    thread1.join()
    if thread1.isAlive():
        continue
    else:
        break
'''